

const useUpdate = (url, methode, bodies) => {
    
    fetch (url, {
        method : methode, 
        headers:
        {
            'Content-Type' : 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(bodies), 
        charset: 'UTF-8',
        
    })
        .then( () => {
            // console.log(token)
            window.location.reload(true)
        })
    
}

export default useUpdate;