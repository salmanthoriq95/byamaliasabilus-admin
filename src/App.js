import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { Home, Nav, Header, Article, Gallery, Profile, Services, Footer, EditArticle, AddArticle, Login, Admin, Logout} from "./components/index";


function getToken() {
    const token = localStorage.getItem('token')
    return token;

}

function App() {
    
    const token = getToken();

    if (!token || token === "undefined") {
        return <Login />
    }
    
    return (
        <Router>
            <div>
                <Header />
                <div className="container-fluid">
                    <div className="row">
                        <Nav />
                        <Switch>
                            <Route exact path="/">
                                <Home />
                            </Route>
                            <Route path="/admin">
                                <Admin />
                            </Route>
                            <Route path="/article/:id">
                                <EditArticle />
                            </Route>

                            <Route path="/addarticle">
                                <AddArticle />
                            </Route>

                            <Route path="/article">
                                <Article />
                            </Route>

                            <Route path="/gallery">
                                <Gallery />
                            </Route>

                            <Route path="/profile">
                                <Profile />
                            </Route>
                            <Route path="/services">
                                <Services />
                            </Route>

                            <Route path="/logout">
                                <Logout />
                            </Route>
                            
                        </Switch>
                        <Footer />
                    </div>
                </div>
                    
                
            </div>
        </Router>
  );
}

export default App;
