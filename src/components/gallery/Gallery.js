//import all needed libraries
import useFetch from "../../controller/useFetch"; //controller to GET data
import UseUpdate from "../../controller/useUpdate"; //controller to PUT, and DELETE data

const locVal = new RegExp(/^[A-Za-z0-9./,\-']{3,30}$/)
const descVal = new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{5,}$/)

//funtion to update, create, and delete data
function clickUpdate(e,id,method) {
	e.preventDefault();
	const obj = {
		img : document.getElementById("img"+id).value,
		desc : document.getElementById("desc"+id).value,
		loc : document.getElementById("loc"+id).value
	}
	
	UseUpdate('http://localhost:3001/gallery?id='+id,method,obj)
} 

//validator function 
function createValidator() {
	const img  = document.getElementById("img").value
	const desc = document.getElementById("desc").value
	const loc  = document.getElementById("loc").value
	const alert = document.getElementById('alert')

	if (img !== "" && desc.match(descVal) &&loc.match(locVal)) {
		document.getElementById('button').classList.remove('disabled')
		alert.classList.remove('d-block')
		alert.classList.add('d-none')
	} else {
		document.getElementById('button').classList.add('disabled')
		alert.classList.remove('d-none')
		alert.classList.add('d-block')
		alert.innerHTML = "Lokasi harap isi dengan 3-30 karakter dan Deskripsi minimal 5 karakter"
	}

	if (img === "" && desc === "" && loc === ""){
		alert.classList.remove('d-block')
		alert.classList.add('d-none')
	}
}

function updateValidator(e, id) {
	const img  = document.getElementById("img"+id).value
	const desc = document.getElementById("desc"+id).value
	const loc  = document.getElementById("loc"+id).value
	const alert = document.getElementById('alert'+id)

	if ((desc === "" || desc.match(descVal)) && (loc === "" || loc.match(locVal))){
		document.getElementById('button'+id).classList.remove('disabled')
	} else {
		document.getElementById('button'+id).classList.add('disabled')
	}

	if (img !== "" && desc.match(descVal) &&loc.match(locVal)) {
		document.getElementById('button').classList.remove('disabled')
		alert.classList.remove('d-block')
		alert.classList.add('d-none')
	} else {
		document.getElementById('button').classList.add('disabled')
		alert.classList.remove('d-none')
		alert.classList.add('d-block')
		alert.innerHTML = "Lokasi harap isi dengan 3-30 karakter dan Deskripsi minimal 5 karakter"
	}

	if (img === "" && desc === "" && loc === ""){
		alert.classList.remove('d-block')
		alert.classList.add('d-none')
	}

}

//Main (default) function
const Gallery = () => {

	const {data: gallery, error, load} = useFetch('http://localhost:3001/gallery');//Fetching (GET) data

	//Render Page
    return (
    	//Gallery Page
        <div className="col-md-9 mb-5 px-md-4">
			{/*HEADER START*/}
			<div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h1 className="h2">Gallery</h1>
				{/*Modal Trigger*/}
				<button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
					Tambah +
				</button>
			</div>
			{/*Modal for Add Image*/}
			<div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
				<div className="modal-dialog modal-dialog-centered">
					<div className="modal-content">
						<div className="modal-header">
							<h5 className="modal-title" id="staticBackdropLabel">Add Image</h5>
							<button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div className="modal-body">
						<form>
							<div id="alert" className="m-2 alert alert-danger d-none text-center" role="alert"></div>
			                <div className="form-group">
			                    <label>URL Foto</label>
			                    <input className="form-control" id="img" placeholder="Enter URL Foto" onChange= {createValidator} />
			                </div>
			                <div className="form-group">
			                    <label >Deskripsi</label>
			                    <textarea rows="5" className="form-control" id="desc" placeholder="Enter Description" onChange= {createValidator} />
			                </div>
			                <div className="form-group mb-3">
			                    <label >Lokasi</label>
			                    <input className="form-control" id="loc" placeholder="Enter The Location" onChange={createValidator} />
			                </div>
			            </form>
						</div>
						<div className="modal-footer">
							<button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
							<button id="button" type="button" className="btn btn-primary disabled" onClick={(e)=>clickUpdate(e,'','POST')}>Add</button>
						</div>
					</div>
				</div>
			</div>
			{/*HEADER END*/}
			{error && <div>{ error }</div> /*When fetching error*/} 
            {load && <div>Loading . . . </div> /*When Fetching Loading*/}
            {gallery && ( /*Fetching Success! Display Page!*/
				<div className="container">
				
					{/*START LOOPING DATA*/}
					{gallery.map(x => (
						/*START CARD CONTAINER*/
						<div className="container-fluid py-3 mt-3 border rounded border-1 mx-auto row" key={x.id}>
							<img className="d-xl-block d-none col-xl-3 ms-n5 w-50 h-auto" src={x.img} alt="gallery"/> {/*Display image only on desktop*/}
							<img className="d-xl-none d-block col-xl-3 ms-n5 w-100 h-auto" src={x.img} alt="gallery"/> {/*Display image on all devices except desktop*/}
							
							<div className="col-xl ms-1 mt-3 row justify-content-end "> {/*form edit*/}
								<div id={"alert"+x.id} className="m-2 alert alert-danger d-none text-center" role="alert"></div>
								<div className="input-group mb-3 mx-auto row">
									<span className="input-group-text pe-5 col-12 col-md-3">URL</span> {/*URL label*/}
									<input type="text" className="form-control col" id={"img"+x.id} placeholder={x.img} onChange={e => updateValidator(e, x.id)}/> {/*URL input*/}
								</div>
								<div className="input-group mb-3 mx-auto row">
									<span className="input-group-text pe-5 col-12 col-md-3">Description</span> {/* Desc label*/}
									<textarea rows="5" type="text" className="form-control col" id={"desc"+x.id} placeholder={x.desc} onChange={e => updateValidator(e, x.id)} /> {/*Desc input*/}
								</div>
								<div className="input-group mb-3 mx-auto row">
									<span className="input-group-text pe-5 col-12 col-md-3">Location</span> {/*Loc label*/}
									<input type="text" className="form-control col" id={"loc"+x.id} placeholder={x.loc} onChange={e => updateValidator(e, x.id)}/> {/*Loc input*/}
								</div>
								<div className="btn-group col-md-5 col-xl-5 col-12" role="group" aria-label="Basic example">
									<button id={"button"+x.id} type="button" className="btn btn-primary disabled" onClick={(e)=>clickUpdate(e,x.id,"PUT")} >Update</button> {/*Update Button*/}
									<button type="button" className="btn btn-secondary" onClick={(e)=>clickUpdate(e,x.id,"DELETE")}>Delete</button> {/*Delete Button*/}
								</div>
							</div>
						</div>
						/*END CARD CONTAINER*/
					))}
					{/*END LOOPING DATA*/}
				</div>
			)}
			
		</div>
    );
}
 
export default Gallery;