const Footer = () => {
    return (
        <div>
            <div className="fixed-bottom fs-6 text-center py-1 fw-light bg-dark text-secondary">Copyright by <a href="gitlab.com/salmanthoriq95" className="link-light">Salman Thoriq Al Farisyi</a></div>
        </div>
    );
}
 
export default Footer;