//importing all needed library
import useFetch from "../../controller/useFetch";
import UseUpdate from "../../controller/useUpdate";
import {Link} from "react-router-dom"

//function to delete data
function deleteHandler (e, id){
    e.preventDefault();
    // console.log('http://localhost:3001/article?id='+id,"DELETE","{}")
	UseUpdate('http://localhost:3001/article?id='+id,"DELETE")
}

//main function
const Article = () => {

	const {data : blog, error, load} = useFetch('http://localhost:3001/article'); //fetch data


    //render data
    return (
        <div className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            {/* START HEADER */}
			<div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h1 className="h2">Blog</h1>
				<a href="/addarticle" className="btn btn-primary btn-lg" tabIndex="-1" role="button" aria-disabled="true">Tambah Artikel +</a>
			</div>
            {/* END HEADER */}
			{error && <div>{ error }</div> /* when fetching error */}
			{load && <div>Loading . . . </div> /* when loading dataa */}
			{blog && ( /* Fetching Success! Show Data! */
				<div className="mb-2 container">
				{blog.map(x=>(  /* Start Looping */
                            <div key={x.id} className="">
                                <div className="row border rounded mb-4 shadow-sm h-md-250 position-relative">
                                    <div className="col p-4 d-flex flex-column ">
                                        <h3 className="mb-0">{x.title}</h3>  {/* title */}
                                        <div className="mb-1 text-muted">{x.date.day} {x.date.month} {x.date.year}</div> {/* date stamp */}
                                        <div className="fst-italic">{x.author}</div>  {/* author */}
                                        <p className="card-text mb-5">{x.short}</p> {/* short Desc */}
                                        <div className="btn-group col-2 " role="group" aria-label="Basic example"> {/* edit and delete form */}
                                            <Link to={"/article/"+x.id}type="button" className="btn btn-primary">Update</Link>
                                            <button type="button" className="btn btn-secondary" onClick={e=>deleteHandler(e,x.id)}>Delete</button>
                                        </div>
                                    </div>
                                    <div className="col-auto d-none d-lg-block"> {/* thumbnail */}
                                        <svg className="bd-placeholder-img my-2" width="200" height="250" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><image href={x.thumbnail} width="200"></image></svg>
                                        
                                    </div>
                                </div>
                            </div>
                        ))}
				</div>
			)}

		</div>


    );
}
 
export default Article;