//import all needed library
import UseUpdate from "../../controller/useUpdate"; //controller to PUT, POST, and DELETE data
import useFetch from "../../controller/useFetch";
import {Link} from "react-router-dom"

const vis = ["!ok","!ok","!ok","!ok","!ok"]
const schema = {
    title: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{5,30}$/),
    short: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{5,}$/),
    author: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{3,30}$/),
    thumbnail: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{2,}$/),
    content: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{5,}$/),
}

//function for add data
function addData (e) {
    e.preventDefault();
    // const val = document.getElementById(idText).value;
    const obj = {
        title : document.getElementById("title").value,
        thumbnail : document.getElementById("thumbnail").value,
        desc : document.getElementById("desc").value,
        author : document.getElementById("author").value,
        content : document.getElementById("content").value,
    }
    // console.log('http://localhost:3001/article?',"POST",obj)
    UseUpdate('http://localhost:3001/article?',"POST",obj)
}

//validation function 
function validator(e, arg, i) {
    const alert = document.getElementById('alert-'+arg)
    const value = document.getElementById(arg).value
    const valid = value.match(schema.[arg])
    
    if (valid || value === ""){
        alert.classList.remove('d-block')
        alert.classList.add('d-none')
        if (valid){
            vis[i] = "ok"
        }
    } else {
        alert.classList.remove('d-none')
        alert.classList.add('d-block')
        vis[i] = "!ok"
    }
    const findit = vis.find(x => x === "!ok")
    if (findit==="!ok"){
        document.getElementById('button').classList.add('disabled')
    } else {
        document.getElementById('button').classList.remove('disabled')
    }
    
}


//main funtion
const AddArticle = () => {

    const {data : blog, error, load} = useFetch('http://localhost:3001/article'); //fetch data

    //render page
    return (
        <div className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            {/* START HEADER */}
			<div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h1 className="h2">Blog</h1>
			</div>
            {/* END HEADER */}
            {error && <div>{ error }</div> /* When fetching error */}
			{load && <div>Loading . . . </div> /* When loading data */}
			{blog && (/* Fetching Success! Show Data */
            // START MODAL
            <div>
            <div className="modal fade show d-block" id="exampleModal">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Last Article</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={()=>document.getElementById("exampleModal").remove("show")}></button>
                        </div>
                        <div className="modal-body">
                            <div className="row border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                                <div className="col p-4 d-flex flex-column position-static ">
                                    <h3 className="mb-0">{blog[0].title}</h3>  {/* title */}
                                    <div className="mb-1 text-muted">{blog[0].date.day} {blog[0].date.month} {blog[0].date.year}</div> {/* date stamp */}
                                    <div className="fst-italic">{blog[0].author}</div>  {/* author */}
                                    <p className="card-text mb-5">{blog[0].short}</p> {/* short Desc */}
                                </div>
                                <div className="col-auto d-none d-lg-block"> {/* thumbnail */}
                                    <svg className="bd-placeholder-img" width="200" height="250" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><image href={blog[0].thumbnail} width="200"></image></svg>
                                    
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={()=>document.getElementById("exampleModal").remove("show")}>Close</button>
                            <Link to="/article" type="button" className="btn btn-primary">Back to Article</Link>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            )}
            {/* END MODAL */}
            {/* START FORM */}
            <form>
                <div className="form-group"> {/* Title Form */}
                    <label >Judul Artikel</label>
                    <div id="alert-title" className="m-2 alert alert-danger d-none text-center" role="alert">
                        Judul harus terdiri dari 5-30 karakter.
                    </div>
                    <input className="form-control" id="title" placeholder="Enter Article Title" onChange={e => validator(e, 'title', 0)} />
                </div>
                <div className="form-group"> {/* Thumbnail Form */}
                    <label>URL thumbnail</label>
                    <div id="alert-thumbnail" className="m-2 alert alert-danger d-none text-center" role="alert">
                        Isi dengan alamat URL gambar thumbnail.
                    </div>
                    <input className="form-control" id="thumbnail" placeholder="Enter URL Thumbnail" onChange={e => validator(e, 'thumbnail', 1)} />
                </div>
                <div className="form-group"> {/* Desc Form */}
                    <label id="desc" >Deskripsi Singkat</label>
                    <div id="alert-short" className="m-2 alert alert-danger d-none text-center" role="alert">
                        Deskripsi Singkat minimal terdiri dari 5 karakter.
                    </div>
                    <input className="form-control" id="short" placeholder="Enter Short Description" onChange={e => validator(e, 'short', 2)} />
                </div>
                <div className="form-group"> {/* Author Form */}
                    <label>Penulis</label>
                    <div id="alert-author" className="m-2 alert alert-danger d-none text-center" role="alert">
                        Nama Penulis harus terdiri dari 3-30 karakter.
                    </div>
                    <input className="form-control"id="author" placeholder="Enter The Author" onChange={e => validator(e, 'author', 3)} />
                </div>
                <div className="form-group mb-3"> {/* Content Form */}
                    <label >Artikel</label>
                    <div id="alert-content" className="m-2 alert alert-danger d-none text-center" role="alert">
                        Konten artikel harus terdiri minimal 5 karakter.
                    </div>
                    <textarea className="form-control" id="content" rows="15" onChange={e => validator(e, 'content', 4)}></textarea>
                </div>
                <button id="button" type="button" className="btn btn-primary btn-lg disabled" onClick={addData} >Selesai</button> {/* Button Enter */}
            </form>
            {/* END FORM */}
        </div>

    );
}
 
export default AddArticle;