//import all needed library
import React from 'react';
import useFetch from "../../controller/useFetch";
import UseUpdate from "../../controller/useUpdate"; //controller to PUT, POST, and DELETE data
import {useParams} from "react-router";
import {ReactComponent as Edit} from "../icon/Edit.svg"
import ReactTooltip from "react-tooltip";

const schema = {
    title: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{5,30}$/),
    short: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{5,}$/),
    author: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{3,30}$/),
    thumbnail: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{2,}$/),
    content: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{5,}$/),
}


//validation function 
function validator(e, arg,) {
    const alert = document.getElementById('alert-'+arg)
    const value = document.getElementById(arg).value
    const valid = value.match(schema.[arg])
    const button = document.getElementById('button-'+arg).classList
    if (valid || value === ""){
        alert.classList.remove('d-block')
        alert.classList.add('d-none')
		if (valid){
			button.remove('disabled')
		}
    } else {
        alert.classList.remove('d-none')
        alert.classList.add('d-block')
		button.add('disabled')
    }
    
}

//function to toggle form edit
function showForm(props){
	let hide = props.currentTarget.parentNode.lastChild;
	hide.classList.toggle('d-none');
}

//funtion to update and delete data
function buttonHandler (e, idText, id, method) {
	e.preventDefault();
	const val = document.getElementById(idText).value;
	const obj = {[idText]:val}
	return UseUpdate('http://localhost:3001/article?id='+id,method,obj)
	
}

//main function
const EditArticle = () => {

	const {id} = useParams(); //call parameter in address bar
	const {data: blog, error, load} = useFetch('http://localhost:3001/article?id='+id); //fetching specific data


	//render page
	return (
		// START RENDER
		<div className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
			{/* HEADER START */}
			<div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h1 className="h2">Blog</h1>
			</div>
			{/* HEADER END */}
			{error && <div>{ error }</div> /* When fetching error */}
			{load && <div>Loading . . . </div> /* When loading data */}
			{blog && ( /* Fetching Success! Show Data! */
				// Start Article
				<main className="container text-start">
					<div className="row">
						<div className="col mb-5">
							<article className="blog-post">
								{/* START TITLE */}
								<h2 className="blog-post-title text-center fw-bold display-4">
									{blog.title}{/* Title */}
									<svg className="d-inline position-absolute" width="16" height="16" data-tip="Edit title" style={{cursor:"pointer"}} onClick={showForm}><Edit /></svg> {/* Edit icon */}
									
									<form className="row d-none"> {/* title form edit */}
										<div id="alert-title" className="m-2 alert alert-danger d-none text-center fs-6" role="alert">
											Judul harus terdiri dari 5-30 karakter.
										</div>
										<input id="title" className="form-control col-md-10" type="text" placeholder="Enter New Title Here" onChange={e => validator(e,'title')}></input>
										<button id="button-title" type="submit" className="btn btn-primary col-md-2 disabled" onClick={(e)=>buttonHandler(e,"title",blog.id,"PUT")}>Update</button>
									</form>
								</h2>
								{/* END TITLE */}
								{/* START AUTHOR */}
								<div className="blog-post-title text-center fst-italic">
									{blog.author}{/* author */}
									<svg className="d-inline ms-2 mb-1" width="16" height="16" data-tip="Edit title" style={{cursor:"pointer"}} onClick={showForm}><Edit /></svg> {/* Edit icon */}
									
									<form className="row d-none"> {/* author form edit */}
										<div id="alert-author" className="m-2 alert alert-danger d-none text-center" role="alert">
											Nama Penulis harus terdiri dari 3-30 karakter.
										</div>
										<input id="author" className="form-control col-md-10" type="text" placeholder="Enter the author" onChange={e => validator(e,'author')}></input>
										<button id="button-author" type="submit" className="btn btn-primary col-md-2 disabled" onClick={(e)=>buttonHandler(e,"author",blog.id,"PUT")}>Update</button>
									</form>
								</div>
								{/* END AUTHOR */}
								{/* START THUMBNAIL */}
								<div className="blog-post-meta text-center">{blog.date.day+" "+blog.date.month+" "+blog.date.year}</div>
								<div className="text-center">
									<svg width="16" height="16" data-tip="Edit image URL" style={{cursor:"pointer"}} onClick={showForm}><Edit /></svg> {/* edit icon */}
									<ReactTooltip />
									<img src={blog.thumbnail} className="img-fluid d-block mx-auto mb-2" alt="thumbnail" style={{width:350}}></img> {/* thumbnail */}
									<form className="row d-none"> {/* form ediit thumbnail */}
										<div id="alert-thumbnail" className="m-2 alert alert-danger d-none text-center" role="alert">
											Isi dengan alamat URL gambar thumbnail.
										</div>
										<input id="thumbnail" className="form-control col-md-10" type="text" placeholder="Enter URL Image" onChange={e => validator(e,'thumbnail')}></input>
										<button id="button-thumbnail" type="submit" className="btn btn-primary col-md-2 disabled" onClick={(e)=>buttonHandler(e,"thumbnail", blog.id, "PUT")}>Update</button>
									</form>
								</div>
								{/* END THUMBNAIL */}
								{/* START SHORT DESCRIPTION */}
								<div className="text-center fst-italic mt-4">
									{blog.short} {/* Short Desc */}
									<svg className="ms-3" width="16" height="16" data-tip="Edit short description" style={{cursor:"pointer"}} onClick={showForm}><Edit /></svg> {/* edit icon */}
									<ReactTooltip />
									<form className="row d-none"> {/* form edit short desc */}
										<div id="alert-short" className="m-2 alert alert-danger d-none text-center" role="alert">
											Deskripsi Singkat minimal terdiri dari 5 karakter.
										</div>
										<textarea id="short" className="form-control col-md-10" type="text" placeholder="Enter New Short Description" onChange={e => validator(e,'short')}/>
										<button id="button-short" type="submit" className="btn btn-primary col-md-2 disabled" onClick={(e)=>buttonHandler(e,"short", blog.id, "PUT")}>Update</button>
									</form>
								</div>
								{/* END SHORT DESCRIPTION */}
								<hr />
								{/* START MAIN ARTICLE */}
								<div>
									{blog.content} {/* main article */}
									<svg className="d-inline" width="16" height="16" data-tip="Edit Article" style={{cursor:"pointer"}} onClick={showForm}><Edit /></svg> {/* edit icon*/}
									<ReactTooltip />
									<form  className="row d-none"> {/* form edit article */}
										<div id="alert-content" className="m-2 alert alert-danger d-none text-center" role="alert">
											Konten artikel harus terdiri minimal 5 karakter.
										</div>
										<textarea id="content" className="form-control col-md-10" type="text" placeholder="Enter New New Article" onChange={e => validator(e,'content')}></textarea>
										<button id="button-content" type="submit" className="btn btn-primary col-md-2 disabled" onClick={(e)=>buttonHandler(e,"content", blog.id, "PUT")}>Update</button>
									</form>
								</div>
								{/* END MAIN ARTICLE */}
							</article>
						</div>
					</div>
					<ReactTooltip />
				</main>
				// End article
			)}
		</div>
	)
}

export default EditArticle;