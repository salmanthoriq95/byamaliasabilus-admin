//import all needed libraries
import { useEffect, useState} from "react";
import UseUpdate from "../../controller/useUpdate"; //controller to PUT, and DELETE data
//funtion to update, create, and delete data
function clickUpdate(e,id,method) {
	e.preventDefault();
	const obj = {
		username : document.getElementById("username"+id).value,
		pass : document.getElementById("pass"+id).value
	}
	
	UseUpdate('http://localhost:3001/admin?id='+id,method,obj)
}

//funtion validator
function validator(e, id) {
	const alert = document.getElementById('alert'+id);
	const passValid = new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{6,}$/);
	const usernameValid = new RegExp('[a-zA-Z0-9]{5,30}')
	const username = document.getElementById('username'+id).value
	const pass = document.getElementById('pass'+id).value
	const button = document.getElementById('button'+id)
	if (pass.match(passValid) && username.match(usernameValid)) {
		alert.classList.remove('d-block')
		alert.classList.add('d-none')
		button.classList.remove('disabled')
		return 
	} else {
		button.classList.add('disabled')
	}
	
	if (username || pass){
		if (!username.match(usernameValid)){
			alert.innerHTML = "Username hanya diperbolehkan menggunakan huruf dan angka, minimal 5 karakter"
			alert.classList.remove('d-none')
			alert.classList.add('d-block')
			if (pass === ""){
				return
			}	
		}
		
		if (!pass.match(passValid)) {
			alert.innerHTML = "Password minimal 6 karakter"
			alert.classList.remove('d-none')
			alert.classList.add('d-block')
			return
		} 


		if (pass.match(passValid) && id) {
			button.classList.remove('disabled')
		}
	}

	if (username === ""){
			alert.classList.remove('d-block')
			alert.classList.add('d-none')
	}
	
	
}

const Admin = () => {

    const [data, setData] = useState(null);
    const [load, setLoad] = useState(true);
    const [error, setError] = useState(null);

    useEffect(()=>{
        
        const abortC = new AbortController();
        const url = "http://localhost:3001/admin"
        fetch(url,{signal: abortC.signal, headers : {'Authorization': `Bearer ${localStorage.getItem('token')}`}})
            .then(res => {
                if(!res.ok){
                    throw Error('could not fetch the data for that resource')
                }
                return res.json();
            })
            .then(data => {
                setData(data.data);
                setLoad(false);
                setError(null);
            })
            .catch(err=>{
                if(err.name === 'AbortError'){
                    console.log('fetch aborted');
                    console.log(err);
                } else {
                    setLoad(false);
                    setError(err.message);
                }
            })
        return ()=>abortC.abort();
    })

    return (
        <div className="col-md-9 mb-5 px-md-4">
            {/*HEADER START*/}
			<div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h1 className="h2">Admin Management</h1>
				{/*Modal Trigger*/}
				<button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
					Tambah +
				</button>
			</div>
			{/*Modal for Add admin*/}
			<div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            
                <div className="modal-dialog modal-dialog-centered">
					<div className="modal-content">
						<div className="modal-header">
							<h5 className="modal-title" id="staticBackdropLabel">Tambah Admin</h5>
							<button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div id="alert" className="m-2 alert alert-danger d-none text-center" role="alert"></div>
						<div className="modal-body">
						<form>
			                <div className="form-group">
			                    <label>Username</label>
			                    <input type="text" className="form-control" id="username" placeholder="masukkan username admin" onChange={e =>validator(e, '')} />
			                </div>
			                <div className="form-group mb-3">
			                    <label >Password</label>
			                    <input type="password" className="form-control" id="pass" placeholder="masukkan password" onChange={e =>validator(e,'')} />
			                </div>
			            </form>
						</div>
						<div className="modal-footer">
							<button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
							<button type="button" id="button" className="btn btn-primary disabled" onClick={(e)=>clickUpdate(e,'','POST')}>Add</button>
						</div>
					</div>
				</div>
			</div>
			{/*HEADER END*/}

            {error && <div>{ error }</div> /*When fetching error*/} 
            {load && <div>Loading . . . </div> /*When Fetching Loading*/}
            {data && ( /*Fetching Success! Display Page!*/
                <div>
                    {/*START LOOPING DATA*/}
					{data.map(x => (
						/*START CARD CONTAINER*/
						<div className="container-fluid py-3 mt-3 border rounded border-1 mx-auto row" key={x.id}>
							
							<div className="col-xl ms-1 mt-3 row justify-content-end "> {/*form edit*/}
								<div id={"alert"+x.id} className="m-2 alert alert-danger d-none text-center" role="alert">		</div>
								<div className="input-group mb-3 mx-auto row">
									<span className="input-group-text pe-5 col-12 col-md-3 col-xl-1" >Id</span> {/*ID label*/}
									<input type="text" className="form-control col" id={"id"+x.id} placeholder={x.id} disabled/> {/*ID input*/}
								</div>
								<div className="input-group mb-3 mx-auto row">
									<span className="input-group-text pe-5 col-12 col-md-3 col-xl-1">Username</span> {/* username label*/}
									<input type="text" className="form-control col" id={"username"+x.id} placeholder={x.username} onChange={e =>validator(e,x.id)} /> {/*username input*/}
								</div>
								<div className="input-group mb-3 mx-auto row">
									<span className="input-group-text pe-5 col-12 col-md-3 col-xl-1">Password</span> {/*Pass label*/}
									<input type="password" className="form-control col" id={"pass"+x.id} onChange={e =>validator(e,x.id)} /> {/*Pass input*/}
								</div>
								<div className="btn-group col-md-5 col-xl-3 col-12" role="group" aria-label="Basic example">
									<button type="button" id={"button"+x.id} className="btn btn-primary disabled" onClick={(e)=>clickUpdate(e,x.id,"PUT")} >Update</button> {/*Update Button*/}
									<button type="button" className="btn btn-secondary" onClick={(e)=>clickUpdate(e,x.id,"DELETE")}>Delete</button> {/*Delete Button*/}
								</div>
							</div>
						</div>
						/*END CARD CONTAINER*/
					))}
					{/*END LOOPING DATA*/}
                </div>
            )}
        </div>
    );
}
 
export default Admin;