import React from 'react';
import { Link } from 'react-router-dom';

const Home = (props) => {
	return (

		<div className="col-md-9 ms-sm-auto mb-5 col-lg-10 px-md-4">
			<div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h1 className="h2">Dashboard</h1>
			</div>
			<div className="container py-xl-4">

				<header className="pb-3 mb-4 border-bottom">
					<Link to="/" className="d-flex align-items-center text-dark text-decoration-none">
						<img src="../logo.png" alt="" className="me-4" />
						<span className="fs-4">Hai Admin</span>
					</Link>
				</header>

				<div className="p-5 mb-4 bg-light rounded-3">
					<div className="container-fluid py-5">
						<h1 className="display-5 fw-bold">Edit Artikel</h1>
						<p className="col-md-8 fs-4">Using a series of utilities, you can create this jumbotron, just like the one in previous versions of Bootstrap. Check out the examples below for how you can remix and restyle it to your liking.</p>
						<Link className="btn btn-primary btn-lg" type="button" to="/article">Goto Article</Link>
					</div>
				</div>

				<div className="row align-items-md-stretch">
					<div className="col-md-6">
						<div className="h-100 p-5 text-white bg-dark rounded-3">
							<h2>Edit Profile</h2>
							<p>Swap the background-color utility and add a `.text-*` color utility to mix up the jumbotron look. Then, mix and match with additional component themes and more.</p>
							<Link className="btn btn-outline-light" to="/profile" type="button">Goto Profile</Link>
						</div>
					</div>
					<div className="col-md-6">
						<div className="h-100 p-5 bg-light border rounded-3">
							<h2>Edit Galeri</h2>
							<p>Or, keep it light and add a border for some added definition to the boundaries of your content. Be sure to look under the hood at the source HTML here as we've adjusted the alignment and sizing of both column's content for equal-height.</p>
							<Link to="/gallery"className="btn btn-outline-secondary" type="button">Goto Gallery</Link>
						</div>
					</div>
				</div>

				<div className="p-5 my-4 bg-light rounded-3">
					<div className="container-fluid py-5">
						<h1 className="display-5 fw-bold">Edit Layanan</h1>
						<p className="col-md-8 fs-4">Using a series of utilities, you can create this jumbotron, just like the one in previous versions of Bootstrap. Check out the examples below for how you can remix and restyle it to your liking.</p>
						<Link className="btn btn-primary btn-lg" type="button" to="/services">Goto Services</Link>
					</div>
				</div>
			</div>
		</div>
	
	)
}

export default Home;