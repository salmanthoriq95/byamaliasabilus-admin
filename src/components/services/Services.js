//import all needed lib
import useFetch from "../../controller/useFetch";
import UseUpdate from "../../controller/useUpdate";
import {ReactComponent as Edit} from "../icon/Edit.svg"
import {ReactComponent as Trash} from "../icon/Trash.svg"
import ReactTooltip from "react-tooltip";

//service schema validator
const serviceSchema = {
	type : new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{3,20}$/),
	desc : new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{5,}$/),
	thumbnail : new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{1,}$/),
}

//package schema validator
const packSchema = {
	thumbnail: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{1,}$/),
	package: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{3,20}$/),
	price: new RegExp(/^[0-9]{1,}$/),
	detail: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{5,}$/),
}

//function to validate package
function packValidate (e, prop, Pid, Sid, v){
	const alert = document.getElementById(Pid+'-alert-'+prop+Sid).classList
	const button = document.getElementById('packButton'+Sid).classList
	const value = document.getElementById(Pid+prop+Sid).value
	const valid = value.match(packSchema.[prop])

	if (value === ""){
		alert.add('d-none')
		alert.remove('d-block')
	} else if (!valid){
		vis[v] = "!ok"
		alert.add('d-block')
		alert.remove('d-none')
	}

	if (valid){
		vis[v] = "ok"
		alert.add('d-none')
		alert.remove('d-block')
	}

	const b = vis.find(x => x==="!ok")
	if (b === '!ok') {
		button.add('disabled')
	} else {
		button.remove('disabled')
	}

}

const vis = []
//function for validate service
function servValidate(e, prop, id, v){
	const alert = document.getElementById('alert-'+prop+id).classList
	const button = document.getElementById('servButton').classList
	const value = document.getElementById(prop+id).value
	const valid = value.match(serviceSchema.[prop])
	if (value === ""){
		alert.add('d-none')
		alert.remove('d-block')
	} else if (!valid){
		vis[v] = "!ok"
		alert.add('d-block')
		alert.remove('d-none')
	}

	if (valid){
		vis[v] = "ok"
		alert.add('d-none')
		alert.remove('d-block')
	}

	const b = vis.find(x => x==="!ok")
	if (b === '!ok') {
		button.add('disabled')
	} else {
		button.remove('disabled')
	}
}



//funtion to show add package form
function showForm(props){
	let hide = props.currentTarget.parentNode.lastChild;
	hide.classList.toggle('d-none');
}

//function to add a service
function addService (e) {
	e.preventDefault();
	const obj ={
		type : document.getElementById("type").value,
		desc : document.getElementById("desc").value,
		thumbnail : document.getElementById("thumb-serv").value,
	}

	UseUpdate('http://localhost:3001/services','POST',obj)
}

//funtion to edit services
function editService (e, key, val, id) {
	e.preventDefault();
	const obj ={
		[key] : document.getElementById(val).value,
	}

	UseUpdate('http://localhost:3001/services?id='+id,'PUT',obj)
}

//function to add package
function addPackage (e,id) {
	e.preventDefault();
	const obj = {
		thumbnail : document.getElementById("thumbnail"+id).value,
		package : document.getElementById("package"+id).value,
		price : document.getElementById("price"+id).value,
		details : document.getElementById("details"+id).value,
	}
	console.log(obj)
	UseUpdate('http://localhost:3001/services?id='+id,'POST',obj)
}

//function to Edit Package
function editPackage (e, xId, yId, method) {
	e.preventDefault();
	const obj = {
		thumbnail : document.getElementById(xId+"thumbnail"+yId).value,
		package : document.getElementById(xId+"package"+yId).value,
		price : document.getElementById(xId+"price"+yId).value,
		details : document.getElementById(xId+"details"+yId).value,
	}
	// console.log (obj)
	UseUpdate('http://localhost:3001/services?id='+xId+'&pack='+yId,method,obj)
}

//function to delete service
function delService (e, id) {
	const confirm = window.confirm("Hapus Layanan?")
	if (confirm) {
		UseUpdate('http://localhost:3001/services?id='+id,'DELETE')
	}
}

const Services = () => {

	const {data : service, load, error} = useFetch('http://localhost:3001/services'); //fetching data
	
	//render data
    return (
        <div className="col-md-9 ms-sm-auto col-lg-10 px-md-4 mb-5">
			{/* START HEADER */}
			<div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h1 className="h2">Services</h1>
				<button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
					Tambah Layanan
				</button>
			</div>
			{/* END HEADER */}
			{/* START MODAL */}
			<div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div className="modal-dialog modal-dialog-centered">
					<div className="modal-content">
						<div className="modal-header">
							<h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
							<button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div className="modal-body">
						<form className="form-group mb-2 p-1" onBlur={e=>e.target.parentNode.parentNode.classList.remove('border','border-1','border-primary')} onFocus={(e)=>e.target.parentNode.parentNode.classList.add('border', 'border-1','border-primary')}>
							<div className="input-group row-md mb-1">
								<div className="input-group-prepend col-3 d-sm-block"><span className="input-group-text">Jenis</span></div>
								<input className="form-control" type="text" placeholder="Nama Layanan" id="type" onChange = {e => servValidate(e, 'type', '', 1)} />
								<div id="alert-type" className=" col-12 form-text text-end fst-italic d-none">*Nama Layanan harus 3-20 karakter.</div>
							</div>
							
							<div className="input-group row-md mb-1">
								<span className="input-group-text col-3">Deskripsi</span>
								<textarea className="form-control" type="text"placeholder="Deskripsi Layanan" id="desc" onChange = {e => servValidate(e, 'desc', '', 2)} />
								<div id="alert-desc" className=" col-12 form-text text-end fst-italic d-none">*Deskripsi Layanan minimal 5 karakter.</div>
							</div>	
							<div className="input-group row-md mb-1">
								<div className="input-group-prepend col-3 d-sm-block"><span className="input-group-text">Thumbnail</span></div>
								<input className="form-control" type="text" placeholder="URL thumbnail" id="thumb-serv" onChange = {e => servValidate(e, 'thumb-serv', '', 3)} />
								<div id="alert-thumb-serv" className=" col-12 form-text text-end fst-italic d-none">*thumbnail harus berisi alamat URL gambar.</div>
							</div>						
						</form>
						</div>
						<div className="modal-footer">
							<button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
							<button id="servButton" type="button" className="btn btn-primary disabled" onClick={addService}>Save changes</button>
						</div>
					</div>
				</div>
			</div>
			{/* END MODAL */}
			{error && <div>{ error }</div> /* When fetching error */}
			{load && <div>Loading . . . </div> /* When loading data */}
			{service && ( /* Fetching Success! Show Data */
				
			service.map( x => ( /* Start Looping the Service */
				// container all packages
				<div key={x.type} className="container">
					{/* Container Head of Packages */}
					<div className="text-center mb-2">
					<button className="btn btn-secondary dropdown-toggle position-absolute end-0 me-md-5 mt-md-2 mt-xl-3" onClick={e=>e.target.parentNode.nextSibling.classList.toggle('d-none')}></button> {/* Trigger to show all packages */}
						{/* start container Name of the package */}
						<h1 className="display-4 fw-normal border-bottom"> 
							{x.type} {/* package name */}
							<svg width="16" height="16" className="ms-md-2" data-tip="Edit Package Name" style={{cursor:"pointer"}} onClick={showForm}><Edit /></svg> {/* Edit icon */}
							<svg width="16" height="16" className="ms-md-2" data-tip="Delete Package Name" style={{cursor:"pointer"}} onClick={e=> delService(e, x.id)}><Trash /></svg> {/* Edit icon */}
							<ReactTooltip />
							<form className="row d-none clearfix px-5"> {/* form edit */}
								<input className="fs-6 col-md-9" type="text" placeholder="Enter Package Name" id={"type"+x.id}></input>
								<button type="submit" className="fs-6 btn btn-primary col-md-3" onClick={e=>editService(e,"type","type"+x.id,x.id)}>Update</button>
							</form>
						</h1>
						{/* End Container Name of the package */}
						{/* Start Container Desc of the package */}
						<div className="fs-5 text-muted">
							{x.desc} {/* Pacakage desc */}
							<svg width="16" height="16" className="ms-2" data-tip="Edit package description" style={{cursor:"pointer"}} onClick={showForm}><Edit /></svg> {/* Edit icon */}
							<ReactTooltip />
							<form className="row d-none px-5"> {/* Form Edit */}
								<input className="fs-6 col-md-9" type="text" placeholder="Enter Package Description" id={"desc"+x.id}></input>
								<button type="submit" className=" fs-6 btn btn-primary col-md-3" onClick={e=>editService(e,"desc","desc"+x.id,x.id)}>Update</button>
							</form>
						</div>
						{/* End Container Desc of Package */}
					</div>
					{/* End Container Head of Pakckages */}
					{/* Start Container Pakckages */}
					<div className="d-none">
						<div className="container form-inline">
							{/* Start Container Add Package Form */}
							<div className="border border-1 p-3 mb-5 " >
								{/* trigger to show add package form*/}
								<button className="btn btn-primary mb-2" onClick={e => e.target.nextSibling.classList.toggle('d-none')}>Tambah Paket +</button> 
								{/* Form add package */}
								<form className="form-group mb-2 p-1 d-none" onBlur={e=>e.target.parentNode.parentNode.classList.remove('border','border-1','border-primary')} onFocus={(e)=>e.target.parentNode.parentNode.classList.add('border', 'border-1','border-primary')}>
									<div className="input-group row-md mb-1">
										<span className="input-group-text col-12 col-md-3 col-xl-2">Paket</span>
										<input className="form-control" type="text" id={"package"+x.id} placeholder="Nama Paket"  onChange={e => packValidate(e, 'package', '', x.id, 1)} />
										<div id={"-alert-package"+x.id} className=" col-12 form-text text-end fst-italic d-none">*Nama Paket harus 3-20 karakter.</div>
									</div>
									<div className="input-group row-md mb-1">
										<span className="input-group-text col-12 col-md-3 col-xl-2">Thumbnail</span>
										<input className="form-control" type="text" id={"thumbnail"+x.id} placeholder="URL Thumbnail" onChange={e => packValidate(e, 'thumbnail', '', x.id, 2)} />
										<div id={"-alert-thumbnail"+x.id} className=" col-12 form-text text-end fst-italic d-none">*thumbnail harus diisi dengan lamat url gamber.</div>
									</div>
									<div className="input-group row-md mb-1">
										<span className="input-group-text col-12 col-md-3 col-xl-2">Deskripsi</span>
										<textarea rows="4" className="form-control" type="text" id={"details"+x.id} placeholder="Deskripsi" onChange={e => packValidate(e, 'details', '', x.id, 3)} />
										<div id={"-alert-details"+x.id} className=" col-12 form-text text-end fst-italic d-none">*Deskripsi minimal 5 karakter.</div>
									</div>
									<div className="input-group row-md mb-1">
										<span className="input-group-text col-12 col-md-3 col-xl-2">Harga</span>
										<input className="form-control" type="text" id={"price"+x.id} placeholder="Harga" onChange={e => packValidate(e, 'price', '', x.id, 4)} />
										<div id={"-alert-price"+x.id} className=" col-12 form-text text-end fst-italic d-none">*Harga harus diisi dengan angka</div>
									</div>
									<div className="container d-inline-flex justify-content-end">
										<div className="btn-group my-2 col-xl-2" role="group" aria-label="Basic example">
											<button id={"packButton"+x.id} className="btn btn-primary disabled" onClick={e=>addPackage(e,x.id)}>Selesai</button>
										</div>
									</div>
								</form>
							</div>
							{/* End Container Add Package Form */}
							
							{x.prices.map((y)=>( /*start looping package */
									// Form Edit package
									
									<form key={x.id+y.package} className="form-group mb-2 p-1" onBlur={e=>e.target.parentNode.parentNode.classList.remove('border','border-1','border-primary')} onFocus={(e)=>e.target.parentNode.parentNode.classList.add('border', 'border-1','border-primary')}>
										<div className="input-group row-md mb-1">
											<span className="input-group-text col-12 col-md-3 col-xl-2">Paket</span>
											<input className="form-control" type="text" id={x.id+"package"+y.id} placeholder={y.package}/>
										</div>
										<div className="input-group row-md mb-1">
											<span className="input-group-text col-12 col-md-3 col-xl-2">Thumbnail</span>
											<input className="form-control" type="text" id={x.id+"thumbnail"+y.id} placeholder={y.thumbnail}/>
										</div>
										<div className="input-group row-md mb-1">
											<span className="input-group-text col-12 col-md-3 col-xl-2">Deskripsi</span>
											<textarea rows="4" className="form-control" type={x.id+"text"+y.id} id={x.id+"details"+y.id} placeholder={y.details}/>
										</div>
										<div className="input-group row-md mb-1">
											<span className="input-group-text col-12 col-md-3 col-xl-2">Harga</span>
											<input className="form-control" type="text" id={x.id+"price"+y.id} placeholder={y.price}/>
										</div>
										<div className="container d-inline-flex justify-content-end">
											<div className="btn-group my-2" role="group" aria-label="Basic example">
												<button className="btn btn-primary " onClick={e => editPackage(e, x.id, y.id,"PUT")}>Update</button>
												<button className="btn btn-secondary" onClick={e => editPackage(e, x.id, y.id,"DELETE")}>Hapus</button>
											</div>
										</div>
										
									</form>
									// End Form
								
							))}
						</div>
					</div>
					{/* End Container Packages */}
				</div>
			))
			)}

		</div>
    );
}
 
export default Services;