import { useState } from "react";


async function loginUser(credentials) {
        return fetch('http://localhost:3001/admin', {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json'
        },
        body: JSON.stringify(credentials)
        })
        .then(data => data.json())
        .then(data => {
      
            // console.log(typeof data.token)
            localStorage.setItem('token', data.token)
        })
    }

const Login = () => {
    const [username, setUsername] = useState();
    const [pass, setPass] = useState();
    
    const submitHandler = async e => {
        e.preventDefault();
        await loginUser({username, pass});
        window.location.reload(true)
    }

    return (
        <div className="px-md-4">
            <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h1 className="h2">Welcome Admin</h1>
			</div>
            <form>
                <div className="mb-3 row">
                    <label className="col-sm-2 col-form-label">Username</label>
                    <div className="col-sm-10">
                        <input className="form-control" id="exampleFormControlInput1" placeholder="Username" onChange={e => setUsername(e.target.value)} />
                    </div>
                </div>
                <div className="mb-3 row">
                    <label className="col-sm-2 col-form-label">Password</label>
                    <div className="col-sm-10">
                        <input type="password" className="form-control" id="inputPassword" placeholder="Password" onChange={e => setPass(e.target.value)}/>
                    </div>
                </div>
                <div className="row justify-content-end">
                    <button type="button" className="btn btn-primary col-3"  onClick={submitHandler}>Login</button>
                </div>
                
            </form>
        </div>
    );
}
 
export default Login;