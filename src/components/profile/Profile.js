//importing all needed library
import useFetch from "../../controller/useFetch"; //controller to GET data
import UseUpdate from "../../controller/useUpdate"; //controller to PUT, POST, and DELETE data
import ReactTooltip from "react-tooltip"; //library for create tooltip
import {ReactComponent as Edit} from "../icon/Edit.svg" //edit icon
import {ReactComponent as Email} from "../icon/Email.svg" //email icon
import {ReactComponent as Wa} from "../icon/Wa.svg" //whatsapp icon
import {ReactComponent as Ig} from "../icon/Ig.svg" //instagram icon
import {ReactComponent as Fb} from "../icon/Fb.svg" //facebook icon
import {ReactComponent as Twitter} from "../icon/Twitter.svg" //twitter icon
import {ReactComponent as Home} from "../icon/Home.svg" //home icon

const schema = {
	name: new RegExp(/^[a-zA-Z'.-]{5,30}$/),
	address: new RegExp(/^[A-Za-z0-9./,\-']{5,}$/),
	desc: new RegExp(/^[a-zA-Z0-9~!@#$%^&*()_+`\-={}|[\]:";'<>?,./]{5,}$/),
	email: new RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]+$/),
	noHp: new RegExp(/^(?:[+]+[0-9]{10,20})$/),
	ig: new RegExp(/^[a-zA-Z0-9'._]{5,30}$/),
	twitter: new RegExp(/^[a-zA-Z0-9'._]{5,30}$/),
	fb: new RegExp(/^[a-zA-Z0-9'._]{5,30}$/),

}


//function to show a form when icon clicked
function showForm(props){
	let hide = props.currentTarget.nextElementSibling;
	hide.classList.toggle('d-none');
}

//validator function
function validator(e, arg) {
	const alert = document.getElementById('alert-'+arg)
	const value = document.getElementById(arg).value
	const valid = value.match(schema.[arg])
	if (valid || value === ""){
		alert.classList.remove('d-block')
		alert.classList.add('d-none')
	} else {
		alert.classList.remove('d-none')
		alert.classList.add('d-add')
	}
}

//main (default) function 
const Profile = () => {

	const {data : profile, error, load} = useFetch('http://localhost:3001/profile'); //fetching original data

	//function to update data
	function clickHandler(e,arg) {
		e.preventDefault();
		const x = document.getElementById(arg).value;
		const obj = {[arg]:x}
		UseUpdate('http://localhost:3001/profile','PUT',obj)
	}
	//Render Page
    return (

		//START PAGE
        <div className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
        	{/*START HEADER*/}
			<div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h1 className="h2">My Profile</h1>
			</div>
			{/*END HEADER*/}

			{error && <div>{ error }</div> /*When Fetching Error*/}
			{load && <div>Loading . . . </div> /* When fetching is loading*/}
			{profile && ( /*Fetching Success! display Page!*/
			<div>
				<div className="p-3 mb-4 bg-light rounded-3">
					{/*START PROFILE PICTURE*/}
					<div className="container-fluid ">
						<div className="row justify-content-center">
							<img className="mb-2 img-fluid rounded-circle col-xl-3 col-md-5 col-sm-3" src={profile[0].picture} alt="profile" width="300" height="300" /> {/*Profile Picture*/}
							<div className="col-sm-1 col-md-1 text-end mb-3" style={{cursor:"pointer"}} onClick={showForm}> {/*Edit icon container*/}
								<svg width="25" height="25" data-tip="Edit your profile picture"><Edit /></svg>
								<ReactTooltip />
							</div>
							<form className="row d-none" > {/*Edit Form*/}
								<input className="border rounded col-md-10"  placeholder="URL Image for Profile Picture" id="picture" />
								<button type="submit" className="btn btn-primary col-md-2 col-12" onClick={(e)=>clickHandler(e,"picture")}>Update</button>
							</form>
						</div>
					</div>
					{/*END PROFILE PICTURE*/}
					{/*START NAME*/}
					<div className="text-center container px-4">
						<h1 className="display-5 fw-bold d-inline">{profile[0].name}</h1> {/*Showing Name*/}
						<span style={{cursor:"pointer"}} onClick={showForm} > {/*Edit icon*/}
							<svg width="30" height="30" data-tip="Edit your name"><Edit /></svg>
							<ReactTooltip />
						</span>
						<form className="row d-none" > {/*Edit form*/}
							<div id="alert-name" className="m-2 alert alert-danger d-none text-center" role="alert">
								Nama harus berupa huruf antara 5-30 karakter, dan karakter lain yang diperbolehkan hanya tanda hubung (-), tanda petik satu ('), dan tanda titik (.).
							</div>
							<input className="border rounded col-md-10"  placeholder="Enter Your Full Name" id="name" onChange={e => validator(e, 'name')} />
							<button type="submit" className="btn btn-primary col-md-2 col-12" onClick={(e)=>clickHandler(e,"name")}>Update</button>
						</form>
					</div>
					{/*END NAME*/}
					{/*START DESCRIPTION*/}
					<div className="text-center mb-5">
						<span className="fs-4">{profile[0].desc}</span> {/*Showing Desc*/}
						<span style={{cursor:"pointer"}} onClick={showForm}> {/*Edit icon*/}
							<svg  width="20" height="20" data-tip="Edit your description"><Edit /></svg>
							<ReactTooltip />
						</span>
						<form className="row d-none" > {/*Edit Form*/}
							<div id="alert-desc" className="m-2 alert alert-danger d-none text-center" role="alert">
								Deskripsi Diri minimal memiliki 5 karakter
							</div>
							<textarea className="form-control col-12" rows="10" placeholder="Fill Your Description" id="desc" onChange={e => validator(e, 'desc')}/>
							<button type="submit" className="btn btn-primary col-12" onClick={(e)=>clickHandler(e,"desc")}>Update</button>
						</form>
					</div>
					{/*END DESCRIPTION*/}
					{/*START EDIT ADDRESS*/}
					<form> {/*form edit*/}
						<div id="alert-address" className="m-2 alert alert-danger d-none text-center" role="alert">
							Alamat minimal memiliki 5 karakter
						</div>
						<div className="input-group mb-3 mx-auto row">
							<span className="input-group-text pe-5 col-12 col-md-3 col-xl-1">Address</span> {/*label*/}
							<textarea rows="5" type="text" className="form-control col" id="address" placeholder={profile[0].address} onChange={e => validator(e, 'address')}/> {/*text input*/}
							<span className="input-group-text col-1 d-none d-sm-block">
								<svg width="20" height="20" className="mt-5"><Home /></svg>{/*email icon*/}
							</span>
							<button onClick={(e)=>clickHandler(e,"address")} type="button" className="btn btn-primary ms-1 col-md-2 col-3">Update</button> {/*button*/}
						</div>
					</form>
					{/*END EDIT ADRESS*/}
					{/*START EDIT EMAIL*/}
					<form> {/*form edit*/}
						<div id="alert-email" className="m-2 alert alert-danger d-none text-center" role="alert">
							Mohon gunakan format email yang benar. <br />  <strong>contoh : email@domain.com</strong>
						</div>
						<div className="input-group mb-3 mx-auto row">
							<span className="input-group-text pe-5 col-12 col-md-3 col-xl-1">Email</span> {/*label*/}
							<input type="text" className="form-control col" id="email" placeholder={profile[0].contact.email} onChange={e => validator(e, 'email')} /> {/*text input*/}
							<span className="input-group-text col-1 d-none d-sm-block">
								<svg width="20" height="20"><Email /></svg>{/*email icon*/}
							</span>
							<button onClick={(e)=>clickHandler(e,"email")} type="button" className="btn btn-primary ms-1 col-md-2 col-3">Update</button> {/*button*/}
						</div>
					</form>
					{/*END EDIT EMAIL*/}
					{/*START EDIT WHATSAPP*/}
					<form> {/*form edit*/}
						<div id="alert-noHp" className="m-2 alert alert-danger d-none text-center" role="alert">
							Mohon gunakan kode negara pada bagian depan nomor. Spasi dan tanda hubung (-) tidak diperbolehkan! <br /> <strong>contoh : +6281234567890</strong>
						</div>
						<div className="input-group mb-3 mx-auto row">
							<span className="input-group-text pe-5 col-12 col-md-3 col-xl-1">Whatsapp</span> {/*label*/}
							<input type="text" className="form-control col" id="noHp" placeholder={profile[0].contact.noHp} onChange={e => validator(e, 'noHp')} /> {/*text input*/}
							<span className="input-group-text col-1 d-none d-sm-block">
								<svg width="20" height="20"><Wa /></svg>{/*whatsapp icon*/}
							</span>
							<button onClick={(e)=>clickHandler(e,"noHp")} type="button" className="btn btn-primary ms-1 col-md-2 col-3">Update</button> {/*button*/}
						</div>
					</form>
					{/*END EDIT WHATSAPP*/}
					{/*START EDIT INSTAGRAM*/}
					<form> {/*form edit*/}
						<div id="alert-ig" className="m-2 alert alert-danger d-none text-center" role="alert">
							Instagram harus memiliki 5-30 karakter, tanpa menggunakan <strong>@</strong> di awal kata.
						</div>
						<div className="input-group mb-3 mx-auto row">
							<span className="input-group-text pe-5 col-12 col-md-3 col-xl-1">Instagram</span> {/*label*/}
							<input type="text" className="form-control col" id="ig" placeholder={profile[0].socmed.ig} onChange={e => validator(e, 'ig')} /> {/*text input*/}
							<span className="input-group-text col-1 d-none d-sm-block">
								<svg width="20" height="20"><Ig /></svg>{/*instagram icon*/}
							</span>
							<button onClick={(e)=>clickHandler(e,"ig")} type="button" className="btn btn-primary ms-1 col-md-2 col-3">Update</button> {/*button*/}
						</div>
					</form>
					{/*END EDIT INSTAGRAM*/}
					{/*START EDIT FACEBOOK*/}
					<form> {/*form edit*/}
						<div id="alert-fb" className="m-2 alert alert-danger d-none text-center" role="alert">
							Facebook harus memiliki 5-30 karakter.
						</div>
						<div className="input-group mb-3 mx-auto row">
							<span className="input-group-text pe-5 col-12 col-md-3 col-xl-1">Facebook</span> {/*label*/}
							<input type="text" className="form-control col" id="fb" placeholder={profile[0].socmed.fb} onChange={e => validator(e, 'fb')} /> {/*text input*/}
							<span className="input-group-text col-1 d-none d-sm-block">
								<svg width="20" height="20"><Fb /></svg>{/*facebook icon*/}
							</span>
							<button onClick={(e)=>clickHandler(e,"fb")} type="button" className="btn btn-primary ms-1 col-md-2 col-3">Update</button> {/*button*/}
						</div>
					</form>
					{/*END EDIT FACEBOOK*/}
					{/*START EDIT TWITTER*/}
					<form> {/*form edit*/}
						<div id="alert-twitter" className="m-2 alert alert-danger d-none text-center" role="alert">
							twitter harus memiliki 5-30 karakter, tanpa menggunakan <strong>@</strong> di awal kata.
						</div>
						<div className="input-group mb-3 mx-auto row">
							<span className="input-group-text pe-5 col-12 col-md-3 col-xl-1">Twitter</span> {/*label*/}
							<input type="text" className="form-control col" id="twitter" placeholder={profile[0].socmed.twitter} onChange={e => validator(e, 'twitter')} /> {/*text input*/}
							<span className="input-group-text col-1 d-none d-sm-block">
								<svg width="20" height="20"><Twitter /></svg>{/*twitter icon*/}
							</span>
							<button onClick={(e)=>clickHandler(e,"twitter")} type="button" className="btn btn-primary ms-1 col-md-2 col-3">Update</button> {/*button*/}
						</div>
					</form>
					{/*END EDIT TWITTER*/}
				</div>	
			</div>
			)}
			
		</div>
    );
}
 
export default Profile;